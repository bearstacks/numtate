//
//  GameStartViewController.swift
//  NumberRotate2
//
//  Created by Zeshan Anwar on 2016-04-27.
//  Copyright © 2016 Zeshan Anwar. All rights reserved.
//

import UIKit

class GameStartViewController: UIViewController {

    @IBOutlet weak var playButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        let grayValue : CGFloat = 40
        self.view.backgroundColor = UIColor.init(red: grayValue/255, green: grayValue/255, blue: grayValue/255, alpha: 1.0)
        playButton.layer.borderWidth = 1.0
        playButton.layer.borderColor = UIColor.whiteColor().CGColor

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func playButtonPressed(sender: AnyObject) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewControllerWithIdentifier("ViewController")
        
        self.presentViewController(viewController, animated: true, completion: {
        })
    }
}
