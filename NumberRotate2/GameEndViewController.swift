//
//  GameEndViewController.swift
//  NumberRotate2
//
//  Created by Zeshan Anwar on 2016-04-27.
//  Copyright © 2016 Zeshan Anwar. All rights reserved.
//

import UIKit

class GameEndViewController: UIViewController {

    var viewController : ViewController!
    @IBOutlet weak var numOfTransformations: UILabel!
    
    @IBOutlet weak var playAgain: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        let grayValue : CGFloat = 40
        self.view.backgroundColor = UIColor.init(red: grayValue/255, green: grayValue/255, blue: grayValue/255, alpha: 1.0)
        playAgain.layer.borderWidth = 1.0
        playAgain.layer.borderColor = UIColor.whiteColor().CGColor
        
        numOfTransformations.text = String(viewController.numOfTransformations)
    }

    @IBAction func playAgain(sender: AnyObject) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewControllerWithIdentifier("ViewController")
        
        self.presentViewController(viewController, animated: true, completion: {
        })
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
