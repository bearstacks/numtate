//
//  ViewController.swift
//  NumberRotate2
//
//  Created by Zeshan Anwar on 2016-04-20.
//  Copyright © 2016 Zeshan Anwar. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {
    
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!
    @IBOutlet weak var label3: UILabel!
    
    @IBOutlet weak var label4: UILabel!
    @IBOutlet weak var label5: UILabel!
    @IBOutlet weak var label6: UILabel!

    @IBOutlet weak var label7: UILabel!
    @IBOutlet weak var label8: UILabel!
    @IBOutlet weak var label9: UILabel!
    
    @IBOutlet weak var ButtonOne: UIButton!
    @IBOutlet weak var ButtonTwo: UIButton!
    @IBOutlet weak var ButtonThree: UIButton!
    @IBOutlet weak var ButtonFour: UIButton!
    var randomizingLabel : UILabel!
    
    var audioPlayer : AVAudioPlayer!
    
    var labelPositioning : [[UILabel]] = [[UILabel]]()
    
    let animationStyle : UIViewAnimationOptions = .CurveLinear
    
    var numOfTransformations : Int = 0
    
    @IBAction func Button1(sender: AnyObject) {
//        playButtonPress()
        RotateButton1()
    }
    
    @IBAction func Button2(sender: AnyObject) {
//        playButtonPress()
        RotateButton2()
    }
    
    @IBAction func Button3(sender: AnyObject) {
//        playButtonPress()
        RotateButton3()
    }
    
    @IBAction func Button4(sender: AnyObject) {
//        playButtonPress()
        RotateButton4()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        label1.text = "1"
        label2.text = "2"
        label3.text = "3"
        label4.text = "4"
        label5.text = "5"
        label6.text = "6"
        label7.text = "7"
        label8.text = "8"
        label9.text = "9"
        
//        self.view.backgroundColor = UIColor.init(red: 41.0/255.0, green: 128.0/255.0, blue: 185.0/255.0, alpha: 1.0)
        let grayValue : CGFloat = 40
        self.view.backgroundColor = UIColor.init(red: grayValue/255, green: grayValue/255, blue: grayValue/255, alpha: 1.0)
        labelPositioning = [[label1, label2, label3], [label4, label5, label6], [label7, label8, label9]]
        ButtonOne.layer.borderWidth = 1.0
        ButtonOne.layer.borderColor = UIColor.whiteColor().CGColor
        ButtonTwo.layer.borderWidth = 1.0
        ButtonTwo.layer.borderColor = UIColor.whiteColor().CGColor
        ButtonThree.layer.borderWidth = 1.0
        ButtonThree.layer.borderColor = UIColor.whiteColor().CGColor
        ButtonFour.layer.borderWidth = 1.0
        ButtonFour.layer.borderColor = UIColor.whiteColor().CGColor
        
        ButtonOne.layer.cornerRadius = ButtonOne.frame.size.height/2
        ButtonTwo.layer.cornerRadius = ButtonTwo.frame.size.height/2
        ButtonThree.layer.cornerRadius = ButtonThree.frame.size.height/2
        ButtonFour.layer.cornerRadius = ButtonFour.frame.size.height/2
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        let backgroundColor         = UIColor.whiteColor()
        let textColor               = UIColor.blackColor()
        
        label1.layer.cornerRadius = label1.frame.size.height/2
        label1.backgroundColor = backgroundColor
        label1.clipsToBounds = true
        label1.textColor = textColor
        label1.layer.borderWidth = 1.0
        label1.layer.borderColor = UIColor.whiteColor().CGColor
        
        label2.layer.cornerRadius = label1.frame.size.height/2
        label2.backgroundColor = backgroundColor
        label2.clipsToBounds = true
        label2.textColor = textColor
        label2.layer.borderWidth = 1.0
        label2.layer.borderColor = UIColor.whiteColor().CGColor

        label3.layer.cornerRadius = label1.frame.size.height/2
        label3.backgroundColor = backgroundColor
        label3.clipsToBounds = true
        label3.textColor = textColor
        label3.layer.borderWidth = 1.0
        label3.layer.borderColor = UIColor.whiteColor().CGColor
        
        label4.layer.cornerRadius = label1.frame.size.height/2
        label4.backgroundColor = backgroundColor
        label4.clipsToBounds = true
        label4.textColor = textColor
        label4.layer.borderWidth = 1.0
        label4.layer.borderColor = UIColor.whiteColor().CGColor
        
        label5.layer.cornerRadius = label1.frame.size.height/2
        label5.backgroundColor = backgroundColor
        label5.clipsToBounds = true
        label5.textColor = textColor
        label5.layer.borderWidth = 1.0
        label5.layer.borderColor = UIColor.whiteColor().CGColor
        
        label6.layer.cornerRadius = label1.frame.size.height/2
        label6.backgroundColor = backgroundColor
        label6.clipsToBounds = true
        label6.textColor = textColor
        label6.layer.borderWidth = 1.0
        label6.layer.borderColor = UIColor.whiteColor().CGColor
        
        label7.layer.cornerRadius = label1.frame.size.height/2
        label7.backgroundColor = backgroundColor
        label7.clipsToBounds = true
        label7.textColor = textColor
        label7.layer.borderWidth = 1.0
        label7.layer.borderColor = UIColor.whiteColor().CGColor
        
        label8.layer.cornerRadius = label1.frame.size.height/2
        label8.backgroundColor = backgroundColor
        label8.clipsToBounds = true
        label8.textColor = textColor
        label8.layer.borderWidth = 1.0
        label8.layer.borderColor = UIColor.whiteColor().CGColor
        
        label9.layer.cornerRadius = label1.frame.size.height/2
        label9.backgroundColor = backgroundColor
        label9.clipsToBounds = true
        label9.textColor = textColor
        label9.layer.borderWidth = 1.0
        label9.layer.borderColor = UIColor.whiteColor().CGColor
    }
    
    override func viewDidAppear(animated: Bool) {
        
        randomize()
    }
    
    func randomize() {
        var numbers = [Int]()
        
        for _ in 0 ..< 8 {
            numbers.append(Int(arc4random_uniform(4) + 1));
        }
        
        for i in 0 ..< numbers.count {
            var removeLabel: Bool = false
            if i == numbers.count-1 {
                removeLabel = true
            }
            if numbers[i] == 1 {
                RotateButton1(Double(0.2*Double(i)), removeLabel: removeLabel)
            } else if numbers[i] == 2 {
                RotateButton2(Double(0.2*Double(i)), removeLabel: removeLabel)
            } else if numbers[i] == 3 {
                RotateButton3(Double(0.2*Double(i)), removeLabel: removeLabel)
            }else if numbers[i] == 4 {
                RotateButton4(Double(0.2*Double(i)), removeLabel: removeLabel)
            }
        }
    }
    
    func winState(){
        let backgroundColor         = UIColor.whiteColor()
        let textColor               = UIColor.blackColor()
        let backgroundColorforCorrect         = UIColor.clearColor()
        let textColorforCorrect                = UIColor.whiteColor()
        numOfTransformations += 1
        
        if (self.labelPositioning[0][0].text == "1" &&
        self.labelPositioning[0][1].text == "2" &&
        self.labelPositioning[0][2].text == "3" &&
        self.labelPositioning[1][0].text == "4" &&
        self.labelPositioning[1][1].text == "5" &&
        self.labelPositioning[1][2].text == "6" &&
        self.labelPositioning[2][0].text == "7" &&
        self.labelPositioning[2][1].text == "8" &&
        self.labelPositioning[2][2].text == "9")
        {
            playSound()
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController = storyboard.instantiateViewControllerWithIdentifier("GameEndViewController") as! GameEndViewController
            viewController.viewController = self
            self.presentViewController(viewController, animated: true, completion: {
            })
            print ("You Win!")
        } else {}
        
        if (self.labelPositioning[0][0].text == "1"){
            label1.backgroundColor = backgroundColor
            label1.textColor = textColor
        } else {
            label1.backgroundColor = backgroundColorforCorrect
            label1.textColor = textColorforCorrect
        }
        if (self.labelPositioning[0][1].text == "2"){
            label2.backgroundColor = backgroundColor
            label2.textColor = textColor
        } else {
            label2.backgroundColor = backgroundColorforCorrect
            label2.textColor = textColorforCorrect
        }
        if (self.labelPositioning[0][2].text == "3"){
            label3.backgroundColor = backgroundColor
            label3.textColor = textColor
        } else {
            label3.backgroundColor = backgroundColorforCorrect
            label3.textColor = textColorforCorrect
        }
        if (self.labelPositioning[1][0].text == "4"){
            label4.backgroundColor = backgroundColor
            label4.textColor = textColor
        } else {
            label4.backgroundColor = backgroundColorforCorrect
            label4.textColor = textColorforCorrect
        }
        if (self.labelPositioning[1][1].text == "5"){
            label5.backgroundColor = backgroundColor
            label5.textColor = textColor
        } else {
            label5.backgroundColor = backgroundColorforCorrect
            label5.textColor = textColorforCorrect
        }
        if (self.labelPositioning[1][2].text == "6"){
            label6.backgroundColor = backgroundColor
            label6.textColor = textColor
        } else {
            label6.backgroundColor = backgroundColorforCorrect
            label6.textColor = textColorforCorrect
        }
        if (self.labelPositioning[2][0].text == "7"){
            label7.backgroundColor = backgroundColor
            label7.textColor = textColor
        } else {
            label7.backgroundColor = backgroundColorforCorrect
            label7.textColor = textColorforCorrect
        }
        if (self.labelPositioning[2][1].text == "8"){
            label8.backgroundColor = backgroundColor
            label8.textColor = textColor
        } else {
            label8.backgroundColor = backgroundColorforCorrect
            label8.textColor = textColorforCorrect
        }
        if (self.labelPositioning[2][2].text == "9"){
            label9.backgroundColor = backgroundColor
            label9.textColor = textColor
        } else {
            label9.backgroundColor = backgroundColorforCorrect
            label9.textColor = textColorforCorrect
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func playSound() {

        let sound = NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource("win", ofType: "wav")!)
        do{
            audioPlayer = try AVAudioPlayer(contentsOfURL:sound)
            audioPlayer.prepareToPlay()
            audioPlayer.play()
        }catch {
            print("Error getting the audio file")
        }
    }
    
    func playButtonPress() {
        
        let sound = NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource("click", ofType: "wav")!)
        do{
            audioPlayer = try AVAudioPlayer(contentsOfURL:sound)
            audioPlayer.prepareToPlay()
            audioPlayer.play()
        }catch {
            print("Error getting the audio file")
        }
    }
    
    func RotateButton1(delay: NSTimeInterval = 0.0, removeLabel: Bool = false) {
        UIView.animateWithDuration(0.5, delay: delay, usingSpringWithDamping: 1, initialSpringVelocity: 3, options: animationStyle, animations: {
            let tempFrame = self.labelPositioning[0][0].frame
            self.labelPositioning[0][0].frame = self.labelPositioning[0][1].frame
            self.labelPositioning[0][1].frame = self.labelPositioning[1][1].frame
            self.labelPositioning[1][1].frame = self.labelPositioning[1][0].frame
            self.labelPositioning[1][0].frame = tempFrame
            
            let tempLabel = self.labelPositioning[0][0]
            self.labelPositioning[0][0] = self.labelPositioning[1][0]
            self.labelPositioning[1][0] = self.labelPositioning[1][1]
            self.labelPositioning[1][1] = self.labelPositioning[0][1]
            self.labelPositioning[0][1] = tempLabel
            
            self.winState()
            }, completion: {
                (completion: Bool) -> Void in
                
        })
    }

    func RotateButton2(delay: NSTimeInterval = 0.0, removeLabel: Bool = false) {
        
        UIView.animateWithDuration(0.5, delay: delay, usingSpringWithDamping: 1, initialSpringVelocity: 2, options: animationStyle, animations: {
            let tempFrame = self.labelPositioning[0][1].frame
            self.labelPositioning[0][1].frame = self.labelPositioning[0][2].frame
            self.labelPositioning[0][2].frame = self.labelPositioning[1][2].frame
            self.labelPositioning[1][2].frame = self.labelPositioning[1][1].frame
            self.labelPositioning[1][1].frame = tempFrame
            
            let tempLabel = self.labelPositioning[0][1]
            self.labelPositioning[0][1] = self.labelPositioning[1][1]
            self.labelPositioning[1][1] = self.labelPositioning[1][2]
            self.labelPositioning[1][2] = self.labelPositioning[0][2]
            self.labelPositioning[0][2] = tempLabel
            
            self.winState()
            
            }, completion: {
                (completion: Bool) -> Void in
        })
    }
    
    func RotateButton3(delay: NSTimeInterval = 0.0, removeLabel: Bool = false) {
        UIView.animateWithDuration(0.5, delay: delay, usingSpringWithDamping: 1, initialSpringVelocity: 7, options: animationStyle, animations: {
            let tempFrame = self.labelPositioning[1][0].frame
            self.labelPositioning[1][0].frame = self.labelPositioning[1][1].frame
            self.labelPositioning[1][1].frame = self.labelPositioning[2][1].frame
            self.labelPositioning[2][1].frame = self.labelPositioning[2][0].frame
            self.labelPositioning[2][0].frame = tempFrame
            
            let tempLabel = self.labelPositioning[1][0]
            self.labelPositioning[1][0] = self.labelPositioning[2][0]
            self.labelPositioning[2][0] = self.labelPositioning[2][1]
            self.labelPositioning[2][1] = self.labelPositioning[1][1]
            self.labelPositioning[1][1] = tempLabel
            
            self.winState()
            
            }, completion: {
                (completion: Bool) -> Void in
        })
    }
    
    func RotateButton4(delay: NSTimeInterval = 0.0, removeLabel: Bool = false) {
        UIView.animateWithDuration(0.5, delay: delay, usingSpringWithDamping: 1, initialSpringVelocity: 7, options: animationStyle, animations: {
            let tempFrame = self.labelPositioning[1][1].frame
            self.labelPositioning[1][1].frame = self.labelPositioning[1][2].frame
            self.labelPositioning[1][2].frame = self.labelPositioning[2][2].frame
            self.labelPositioning[2][2].frame = self.labelPositioning[2][1].frame
            self.labelPositioning[2][1].frame = tempFrame
            
            let tempLabel = self.labelPositioning[1][1]
            self.labelPositioning[1][1] = self.labelPositioning[2][1]
            self.labelPositioning[2][1] = self.labelPositioning[2][2]
            self.labelPositioning[2][2] = self.labelPositioning[1][2]
            self.labelPositioning[1][2] = tempLabel
            
            self.winState()
            }, completion: {
                (completion: Bool) -> Void in
        })
    }
}
